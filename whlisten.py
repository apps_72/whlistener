#!/usr/bin/env python3.9
import pika
import os
import time
import json
import argparse
import sys
import re
import subprocess
import psycopg2
import mysql.connector
import jsontree
import datetime
import logging
import sentry_sdk
from flask import Flask
from flask import Flask, request, current_app, g, jsonify, request_finished, make_response, abort
from sentry_sdk.integrations.logging import LoggingIntegration
import tornado
import tornado.web
import tornado.options
from ipaddress import IPv4Address
import ipaddr, ipaddress
import tempfile
import uuid
import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from urllib.parse import urlencode
from urllib.request import Request, urlopen
import requests
import memorize
from json import dumps

import sys  
sys.path.append('./devops.libs')
from telegram_send import telegram_send 

if os.environ.get('THREADS'):
    threads = int(os.environ['THREADS'])
else:
    threads = 0

app = Flask(__name__)
#------------------
#-----CONF PARAMS--
treads_count_started = 3
now = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
formatted_command = "empty"
log_file_name = "empty"
version = "1.0.0"
executor = os.uname()[1]

#-----------------
#-----VARS ENV----
HTTP_HOST_INCOMING = os.getenv('HTTP_HOST_INCOMING', 'whlisten.linux2be.com')
TELEGRAM_BOT_TOKEN = os.getenv('TELEGRAM_BOT_TOKEN', 'dsdsdsdsdgggf3423')
TELEGRAM_CHANNEL_ID = os.getenv('TELEGRAM_CHANNEL_ID', '-2323232323232')
ENV_DEBUG      = os.getenv('ENV_DEBUG', 'True')

logging.getLogger().addHandler(logging.StreamHandler())

def get_logger():
    logger = logging.getLogger("threading_example")
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler("threading.log")
    fmt = '%(asctime)s - %(threadName)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)

    logger.addHandler(fh)
    return logger


def send_post(url,r_content_type,headers,payload):
    if r_content_type  == "application/json":
        r = requests.post(url,data=json.dumps(payload),headers=headers)
    else:
        print("send_post with no json " + str(r_content_type))
        print('headers = ' + str(headers))
        r = requests.post(url,data=payload,headers=headers)
    if r.status_code == 200:
        print('Success! ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    else:
        print('Error. ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    
    return r_data

def send_get(url,r_content_type,r_data):
    get_args = r_data['args']
    headers = r_data['headers']
    print("Exec query headers")
    print(headers)
    print("Exec query get_args")
    print(get_args)
    r = requests.get(url,params=get_args,headers=headers)
    print("r.status_code = " +  str(r.status_code))
    if r.status_code == 200:
        print('Success! ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    else:
        print('Error. ' + str(r.reason))
        r_data = json.dumps({"r_code":r.status_code,"r_reason":str(r.reason),"r_content":str(r.content),"r_text":str(r.text),"r_headers":dict(r.headers) })
    return r_data



def save_request(uuid, request):
    req_data = {}

    req_data['uuid'] = uuid
    req_data['endpoint'] = request.endpoint
    req_data['method'] = request.method
    req_data['Content-Type'] = request.content_type
    req_data['headers'] = dict(request.headers)
    req_data['headers']['X-Auth'] = dict(request.headers)['Remote-Addr']
    req_data['headers']['Host'] = HTTP_HOST_OUTGOING
    req_data['headers']['http_host_origin'] = req_data['headers']['Host'].split(":", 1)[0]
    req_data['headers']['http_host_target'] = HTTP_HOST_OUTGOING
    req_data['remote_addr'] = req_data['headers']['Remote-Addr']
    req_data['headers'].pop('Cookie', None)
    if req_data['method'] == "POST" and req_data['Content-Type'] == "application/json":
        req_data['data'] = request.get_json(force = True)
        req_data['args'] = request.args
        print(f"{request.args=}")
    elif req_data['method'] == "GET":
        req_data['args'] = request.args
        print(f"{request.args=}")
    elif req_data['method'] == "POST" and req_data['Content-Type'] != "application/json":
        req_data['args'] = request.args
        req_data['data_plain'] = request.form
    print("SAVE_R")
    return req_data

def save_response(uuid, resp):
    resp_data = {}
    resp_data['uuid'] = uuid
    resp_data['status_code'] = resp.status_code
    resp_data['status'] = resp.status
    resp_data['headers'] = dict(resp.headers)
    resp_data['data'] = resp.response

    return resp_data


@app.before_request
def before_request():
    g.uuid = str(uuid.uuid4())


@app.before_request
def block_method():
    ip = request.headers['Remote-Addr']
    m_http_host = request.headers['Host'].split(":", 1)
    m_http_host = m_http_host[0]

@app.after_request
def after_request(resp):
    resp.headers.add('Access-Control-Allow-Origin', '*')
    resp.headers.add('Access-Control-Allow-Headers', 'Content-Type, X-Token, X-Auth')
    resp.headers.add('Access-Control-Allow-Methods', 'GET, POST')
    resp_data = save_response(g.uuid, resp)
    return resp

@app.route('/', methods=['GET', 'POST'])

@app.route('/api/sms/healthcheck/front', methods=['GET', 'POST'])
def web_healthcheck():
    data = {'message': 'online', 'status': 'healhy'}
    return make_response(jsonify(data), 200)


@app.route('/gitlab', methods=['GET', 'POST'])
def get_hooks_gitlab():
    r_data = request.get_json(force = True)
    r_full_path=  request.full_path
    print(r_data)

    if r_data['object_kind'] == 'pipeline' and r_data['object_attributes']['status'] == 'failed':
        g_pipeline_failed_url = str(r_data['project']['web_url'])  + '/-/jobs/' + str(r_data['builds'][0]['id'])
        g_pipeline_vars = r_data['object_attributes']['variables']
        t_text = "Pipeline failed %(g_pipeline_failed_url)s. \
        Variables %(g_pipeline_vars)s \
        " % {\
        "g_pipeline_failed_url": str(g_pipeline_failed_url), \
        "g_pipeline_vars": g_pipeline_vars} 
        telegram_send(TELEGRAM_BOT_TOKEN, TELEGRAM_CHANNEL_ID, t_text)
    else:
        print("GGG")

    #print(r_data['object_attributes']['variables'])
    data = {'route': '/hooks'}
    return make_response(jsonify(data), 200)

@app.route("/hooks/<slack_token_p1>/<slack_token_p2>", methods=['GET', 'POST'])
def get_hooks_slack(slack_token_p1,slack_token_p2):
    slack_token = slack_token_p1 + '/' + slack_token_p2
    print(str(slack_token))
    return "The slack_token is " + str(slack_token)
    data = {'route': '/hooks', 'slack_token': slack_token}
    return make_response(jsonify(data), 200)

def main_route():
    print("-------------------------------" + " R_START " + "-------------------------------")

    req_data = save_request(g.uuid, request)
    resp = json.dumps(req_data, indent=4)
    m_http_host = req_data['headers']['Host'].split(":", 1)[0]
    r_data = json.loads(resp)
    print("HTTP_HOST_INCOMING = " + HTTP_HOST_INCOMING)
    print("HTTP_HOST_OUTGOING = " + HTTP_HOST_OUTGOING)
    print("HTTP_Content_Type = " + str(req_data['Content-Type']))

    print("BEGIN-------------------------------req_data.data---------------------------")
    print("route = " + str(req_data['endpoint']))
    print(req_data)
    print("END-------------------------------req_data.data---------------------------")
    try:
        if req_data['method'] == "POST"  and req_data['Content-Type'] == "application/json" and req_data['headers']['X-Gitlab-Token'] is not None:
            print("APP=GITLAB")
        if 'hook' in req_data['endpoint'] and req_data['Content-Type'] == "application/x-www-form-urlencoded":
            print("APP=SLACK")        
    except NameError:
        message = "Error"
    print("-------------------------------" + " R_END " + "-------------------------------")
    return r_data 

    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=ENV_DEBUG)
